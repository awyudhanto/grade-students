const { execSync } = require('child_process')
const fs = require('fs')
const checkRestriction = require('hacktiv8-restriction')
const solutionPath = '../index.js'

const reconstructedFilename = 'reconstructed.js'

const grade = (nama, nilai) => {
  let solution = fs.readFileSync('./index.js', 'utf-8')

  solution = solution.replace(
    /(let|var) nama .*/,
    // to handle undefined or null, it should not be quoted
    `$1 nama = ${typeof nama === 'string' ? `"${nama}"` : nama}`
  )
  solution = solution.replace(/(let|var) nilai .*/, `$1 nilai = ${nilai}`)

  fs.writeFileSync(reconstructedFilename, solution)

  return String(execSync(`node ${reconstructedFilename}`))
}

const correctGrade = (nama, nilai) => {
  let grade;
  if (!nama || nilai > 100 || nilai < 0) {
    return `Nilai Invalid`
  } else if (nilai >= 80) {
    grade = "A"
  } else if (nilai >= 65) {
    grade = "B"
  } else if (nilai >= 50) {
    grade = "C"
  } else if (nilai >= 35) {
    grade = "D"
  } else {
    grade = "E"
  }
  return { nama, grade }
}

afterAll(() => {
  if (fs.existsSync(reconstructedFilename)) {
    fs.unlinkSync(reconstructedFilename)
  }
})

describe('Grade Students', () => {
  describe('Check for restrictions use', () => {
    it('Should not use any built-in functions', async () => {
      const restrictedUse = await checkRestriction(solutionPath)
      expect(restrictedUse).toBe(null)
    })
  })
  describe('Case name null, undefined, ""', () => {
    it('should show "Nilai Invalid" when name = ""', () => {
      const nama = ""
      const nilai = 70
      expect(grade(nama, nilai)).toMatch(correctGrade(nama, nilai))
    })
    it('should show "Nilai Invalid" when name = null', () => {
      const nama = null
      const nilai = 70
      expect(grade(nama, nilai)).toMatch(correctGrade(nama, nilai))
    })
    it('should show "Nilai Invalid" when name = undefined', () => {
      const nama = undefined
      const nilai = 70
      expect(grade(nama, nilai)).toMatch(correctGrade(nama, nilai))
    })
  })

  describe('Case nilai out of range 0-100', () => {
    it('should show "Nilai Invalid" when nilai < 0', () => {
      const nama = "Andhika"
      const nilai = -1
      expect(grade(nama, nilai)).toMatch(correctGrade(nama, nilai))
    })
    it('should show "Nilai Invalid" when nilai > 100', () => {
      const nama = "Andhika"
      const nilai = 101
      expect(grade(nama, nilai)).toMatch(correctGrade(nama, nilai))
    })
  })

  describe('Success Case', () => {
    it('should show name and grade "Andhika A" when nilai = 100', () => {
      const nama = "Andhika"
      const nilai = 100
      let hasilStudent = grade(nama, nilai).replace(/\./g, '')
      const hasilBenar = correctGrade(nama, nilai) //{nama, grade}

      const n = hasilStudent.search(nama)
      expect(hasilStudent.substring(n, n + nama.length)).toEqual(nama)

      let arr = hasilStudent.substring(n + nama.length, hasilStudent.length).split(/[;:=, \n\s]/g)
      expect(arr.indexOf(hasilBenar.grade)).toBeGreaterThan(-1)
    })
    it('should show name and grade "Anna A" when nilai = 80', () => {
      const nama = "Anna"
      const nilai = 80
      let hasilStudent = grade(nama, nilai).replace(/\./g, '')
      const hasilBenar = correctGrade(nama, nilai) //{nama, grade}

      const n = hasilStudent.search(nama)
      expect(hasilStudent.substring(n, n + nama.length)).toEqual(nama)

      let arr = hasilStudent.substring(n + nama.length, hasilStudent.length).split(/[;:=, \n\s]/g)
      expect(arr.indexOf(hasilBenar.grade)).toBeGreaterThan(-1)
    })
    it('should show name and grade "Budi B" when nilai = 65', () => {
      const nama = "Budi"
      const nilai = 100
      let hasilStudent = grade(nama, nilai).replace(/\./g, '')
      const hasilBenar = correctGrade(nama, nilai) //{nama, grade}

      const n = hasilStudent.search(nama)
      expect(hasilStudent.substring(n, n + nama.length)).toEqual(nama)

      let arr = hasilStudent.substring(n + nama.length, hasilStudent.length).split(/[;:=, \n\s]/g)
      expect(arr.indexOf(hasilBenar.grade)).toBeGreaterThan(-1)
    })
    it('should show name and grade "Cara C" when nilai = 50', () => {
      const nama = "Cara"
      const nilai = 100
      let hasilStudent = grade(nama, nilai).replace(/\./g, '')
      const hasilBenar = correctGrade(nama, nilai) //{nama, grade}

      const n = hasilStudent.search(nama)
      expect(hasilStudent.substring(n, n + nama.length)).toEqual(nama)

      let arr = hasilStudent.substring(n + nama.length, hasilStudent.length).split(/[;:=, \n\s]/g)
      expect(arr.indexOf(hasilBenar.grade)).toBeGreaterThan(-1)
    })
    it('should show name and grade "Dennis D" when nilai = 35', () => {
      const nama = "Dennis"
      const nilai = 100
      let hasilStudent = grade(nama, nilai).replace(/\./g, '')
      const hasilBenar = correctGrade(nama, nilai) //{nama, grade}

      const n = hasilStudent.search(nama)
      expect(hasilStudent.substring(n, n + nama.length)).toEqual(nama)

      let arr = hasilStudent.substring(n + nama.length, hasilStudent.length).split(/[;:=, \n\s]/g)
      expect(arr.indexOf(hasilBenar.grade)).toBeGreaterThan(-1)
    })
    it('should show name and grade "Evanti E" when nilai = 15', () => {
      const nama = "Evanti"
      const nilai = 100
      let hasilStudent = grade(nama, nilai).replace(/\./g, '')
      const hasilBenar = correctGrade(nama, nilai) //{nama, grade}

      const n = hasilStudent.search(nama)
      expect(hasilStudent.substring(n, n + nama.length)).toEqual(nama)

      let arr = hasilStudent.substring(n + nama.length, hasilStudent.length).split(/[;:=, \n\s]/g)
      expect(arr.indexOf(hasilBenar.grade)).toBeGreaterThan(-1)
    })
    it('should show name and grade "End E" when nilai = 0', () => {
      const nama = "End"
      const nilai = 0
      let hasilStudent = grade(nama, nilai).replace(/\./g, '')
      const hasilBenar = correctGrade(nama, nilai) //{nama, grade}

      const n = hasilStudent.search(nama)
      expect(hasilStudent.substring(n, n + nama.length)).toEqual(nama)

      let arr = hasilStudent.substring(n + nama.length, hasilStudent.length).split(/[;:=, \n\s]/g)
      expect(arr.indexOf(hasilBenar.grade)).toBeGreaterThan(-1)
    })
  })
})
